%define Rversion %(echo %{version} | sed -e 's/\\./_/g' -e 's/^/R_/')
Name:           expat
Version:        2.6.4
Release:        1
Summary:        An XML parser library
License:        MIT
URL:            https://libexpat.github.io/
Source0:        https://github.com/libexpat/libexpat/releases/download/%{Rversion}/expat-%{version}.tar.xz

BuildRequires:  sed,autoconf,automake,gcc-c++,libtool,xmlto,make
BuildRequires:  cmake-rpm-macros

%description
expat is a stream-oriented XML parser library written in C.
expat excels with files too large to fit RAM, and where
performance and flexibility are crucial.

%package devel
Summary:        Development files
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}
Obsoletes:      %{name}-static < %{version}-%{release}

%description devel
This package provides with static libraries and  header files for developing with expat.

%package_help

%prep
%autosetup -p1

%build
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
export DOCBOOK_TO_MAN="xmlto man"
%configure --enable-static
%make_build

%install
%make_install
%delete_la

rm -fv %{buildroot}%{_datadir}/doc/%{name}/changelog

%check
%make_build check

%files
%doc AUTHORS Changes
%license COPYING
%{_bindir}/*
%{_libdir}/libexpat.so.1*

%files devel
%{_includedir}/*
%{_libdir}/libexpat.a
%{_libdir}/libexpat.so
%{_libdir}/cmake/expat-%{version}
%{_libdir}/pkgconfig/expat.pc

%files help
%doc README.md
%{_mandir}/man1/*

%changelog
* Sat Nov 09 2024 Funda Wang <fundawang@yeah.net> - 2.6.4-1
- update to 2.6.4

* Tue Oct 29 2024 liningjie <liningjie@xfusion.com> - 2.6.3-2
- fix CVE-2024-50602

* Wed Sep 04 2024 Funda Wang <fundawang@yeah.net> - 2.6.3-1
- update to 2.6.3

* Tue Jul 2 2024 warlcok <hunan@kylinos.cn> -2.6.2-1
- update to 2.6.2

* Fri Mar 22 2024 zhangxingrong <zhangxingrong@uniontech.com> - 2.6.0-2
- Fix CVE-2024-28757

* Wed Feb 21 2024 liweigang <izmirvii@gmail.com> - 2.6.0-1
- expat update to 2.6.0

* Tue Jan 31 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 2.5.0-1
- expat update to 2.5.0

* Tue Dec 13 2022 zhoupengcheng <zhoupengcheng11@huawei.com> - 2.4.8-5
- Move autoreconf to build

* Sat Oct 29 2022 fuanan <fuanan3@h-partners.com> - 2.4.8-4
- fix CVE-2022-43680

* Thu Sep 15 2022 panxiaohe <panxh.life@foxmail.com> - 2.4.8-3
- add test for CVE-2022-40674

* Thu Sep 15 2022 dillon chen<dillon.chen@gmail.com> - 2.4.8-2
- fix CVE-2022-40674

* Fri Jul 1 2022 panxiaohe <panxh.life@foxmail.com> - 2.4.8-1
- update to 2.4.8

* Mon Mar 7 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 2.4.6-2
- Relax fix to CVE-2022-25236

* Sat Feb 26 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 2.4.6-1
- update to 2.4.6
- fix CVE-2022-25235 CVE-2022-25236 CVE-2022-25313 CVE-2022-25314 CVE-2022-25315

* Mon Feb 7 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 2.4.4-1
- update to 2.4.4
- fix CVE-2022-23852 CVE-2022-23990

* Mon Jan 17 2022 wangjie <wangjie375@huawei.com> - 2.4.1-2
- Type:CVE
- ID:CVE-2021-45960 CVE-2021-46143 CVE-2022-22822 CVE-2022-22823 CVE-2022-22824 CVE-2022-22825 CVE-2022-22826 CVE-2022-22827
- SUG:NA
- DESC:fix CVE-2021-45960 CVE-2021-46143
       CVE-2022-22822 CVE-2022-22823 CVE-2022-22824 CVE-2022-22825 CVE-2022-22826 CVE-2022-22827

* Tue Jul 6 2021 panxiaohe <panxiaohe@huawei.com> - 2.4.1-1
- update to 2.4.1
- fix CVE-2013-0340

* Wed Jan 20 2021 wangchen <wangchen137@huawei.com> - 2.2.10-1
- update to 2.2.10

* Sun Jun 28 2020 liuchenguang <liuchenguang4@huawei.com> - 2.2.9-2
- quality enhancement synchronization github patch

* Mon May 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.2.9-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 2.2.9

* Mon Oct 21 2019 shenyangyang <shenyangyang4@huawei.com> - 2.2.6-5
- Type:NA
- ID:NA
- SUG:NA
- DESC:modify the directory of AUTHORS

* Mon Oct 21 2019 shenyangyang <shenyangyang4@huawei.com> - 2.2.6-4
- Type:NA
- ID:NA
- SUG:NA
- DESC:move AUTHORS to license directory

* Sat Sep 28 2019 shenyangyang<shenyangyang4@huawei.com> - 2.2.6-3
- Type:cves
- ID:CVE-2019-15903
- SUG:NA
- DESC:fix CVE-2019-15903

* Fri Aug 30 2019 gulining<gulining1@huawei.com> - 2.2.6-2
- Type:cves
- ID:CVE-2018-20843
- SUG:NA
- DESC:fix CVE-2018-20843

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.2.6-1
- Package Init
